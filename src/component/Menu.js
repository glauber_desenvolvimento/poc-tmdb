import React from 'react';
import Fetch from "../services/Fetch";
import { Link } from "react-router-dom";
import logo from "./../images/logo.png"
import searchIcon from "./../images/search.svg"
import caret from "./../images/caret.svg"
import './Menu.scss';

class Menu extends React.Component {

    constructor(props){
        super(props);
        this.state = {items: []}
        this.onClickCategory = this.onClickCategory.bind(this);
        this.onKeyUpSearch = this.onKeyUpSearch.bind(this);
        this.timeout = setTimeout(()=>{}, 400);
    }
    onKeyUpSearch(event){
        let self = this;
        let query = event.target.value;
        clearTimeout(self.timeout);
        self.timeout = setTimeout(()=>{
            if(self.timeout){
                self.props.parent.getByQuery(query);
            }
        }, 300);
    }
    onClickCategory(event){
        let id = event.target.getAttribute("data-id");
        this.props.parent.getByCategory(id);
    }
    componentDidMount(){
        let self = this;
        let f = new Fetch();
        f.get("genre/movie/list").then((response) => {
            response.genres.unshift({
                id: 0,
                name: "All"
            });
            self.setState({
                items: response.genres
            });
        });
    }
    render(){
        let self = this;
        return (
            <nav id='menu'>
                <ul>
                    <li>
                        <Link to="/">
                            <img alt="logo" src={logo} width="150"></img>
                            <b>&nbsp; Lite</b>
                        </Link>
                    </li>
                </ul>
                <ul>
                    <li>
                        <label id="search">
                            <input name="search" onKeyUp={self.onKeyUpSearch}/>
                            <img alt="logo" src={searchIcon} width="20"></img>
                        </label>
                        &nbsp;&nbsp;&nbsp;
                    </li>
                    <li>
                        <a href="/" style={{cursor: "pointer"}}>
                            Gêneros &nbsp;
                            <img alt="logo" src={caret} width="20"></img>
                            <span>
                                <ul>
                                    {self.state.items.map((item,index) => {
                                        return (
                                        <   li key={index} className='category' onClick={self.onClickCategory} data-id={item.id}>{item.name}</li>
                                        )
                                    })}
                                </ul>
                            </span>
                        </a>
                    </li>
                </ul>
            </nav>
        )

    }
}

export default Menu;