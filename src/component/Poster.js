import React from 'react';
// import Fetch from "../services/Fetch";
import './Poster.scss';
import play from "./../images/play.svg"

class Movies extends React.Component {
    render(){
        let self = this;
        return (
            <div id='poster' style={{backgroundImage: "url("+(self.props.item.backdrop_path ? "https://image.tmdb.org/t/p/w500/"+self.props.item.backdrop_path : '')+")"}}>
                <div></div>
                <span>
                    <h1>{self.props.item.title}</h1>
                    <p>{self.props.item.overview}</p>
                    <br/>
                    <a href="/" className="see-more">
                        <img alt={self.props.item.title} src={play} width="20"></img>
                        &nbsp;
                        <span>Ver</span>
                    </a>
                </span>
            </div>
        )

    }
}

export default Movies;