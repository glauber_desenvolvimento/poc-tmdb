import React from 'react';
// import Fetch from "../services/Fetch";
import { Link } from "react-router-dom";
import './Movies.scss';

class Movies extends React.Component {
    constructor(props){
        super(props);
        this.handleState = this.handleState.bind(this);
    }
    handleState(){
        let self = this;
        window.scrollTo(0, 0);
        self.props.parent.getById();
    }
    render(){
        let self = this;
        return (
            <div>
                <div className="movie-container">
                    {self.props.list.map((movie, index)=>{
                    return (
                        <div key={index} className="movie" onClick={self.handleState}>
                            <Link to={'/movie/'+movie.id}>
                                <div className="overlay">
                                    <b>{movie.title}</b>
                                </div>
                                <div style={{backgroundImage: "url(https://image.tmdb.org/t/p/w500/"+movie.backdrop_path+")"}}>
                                </div>
                            </Link>
                        </div>
                    )
                    })}
                </div>
            </div>
        )

    }
}

export default Movies;