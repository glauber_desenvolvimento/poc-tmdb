import React from "react";
import Discover from "../Discover";
// import { Route, Switch, BrowserRouter as Router } from "react-router-dom";
import { Route, Switch, HashRouter as Router } from "react-router-dom";

const RoutesConfig = () => (
  <Router>
    <Switch>
      <Route path="/" component={Discover} exact />
      <Route path="/movie/:id" component={Discover}></Route>
    </Switch>
  </Router>
);

export default RoutesConfig;
