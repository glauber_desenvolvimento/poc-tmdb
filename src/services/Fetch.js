import axios from 'axios';

class Fetch {
    constructor() {
        this.origin = "https://api.themoviedb.org/3/";
        this.key = "?api_key=8d9cdf92ab4d2048ca46c17b3ef8101c";
        this.header = {
            "Content-Type": "application/json;charset=utf-8"
        };
        axios.defaults.headers.common = this.header;
        axios.defaults.baseURL = this.origin;
    }

    get(url){
        return axios.get(url+this.key).then((response) => {
            return Promise.resolve(response.data)
        });
    }

    post(url, data){
        return axios.post(url+this.key, data).then((response) => {
            return Promise.resolve(response.data)
        });
    }

    put(url, data){
        return axios.put(url+this.key, data).then((response) => {
            return Promise.resolve(response.data)
        });
    }

    delete(url){
        return axios.delete(url+this.key).then((response) => {
            return Promise.resolve(response.data)
        });
    }
    
}

export default Fetch;