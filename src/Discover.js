import React from 'react';
import Fetch from "./services/Fetch";
import Movies from "./component/Movies";
import Poster from "./component/Poster";
import Menu from "./component/Menu";
import './Discover.scss';

class Discover extends React.Component {
  
  constructor(props){
    super(props);
    this.state = {
      poster: {
        backdrop_path: ''
      },
      movies: []
    }
    this.movies = this.state.movies;
    this.getByCategory = this.getByCategory.bind(this);
    this.getByQuery = this.getByQuery.bind(this);
    this.getById = this.getById.bind(this);
  }

  getById(){
    let self = this;
    setTimeout(()=>{
      if(self.props.match.params.id){
        //BY ID
        let f = new Fetch();
        f.get("movie/"+self.props.match.params.id).then((response) => {
          self.setState({
            poster: response,
          });
        });
      }
    }, 50);
  }

  getByQuery(query){
    let self = this;
    let toQuery = function(q){
      return q.toUpperCase().replace(/ /g, '');
    }
    query = toQuery(query);
    if(query){
      let movies = self.movies.filter((movie) => {
        var title = toQuery(movie.title);
        return (title.includes(query));
      });
      self.setState({
        movies: movies
      });
    }else{
      self.setState({
        movies: self.movies
      });
    }
  }

  getByCategory(id){
    let self = this;
    if(id>0){
      let movies = self.movies.filter((movie) => {
        let ret = false;
        movie.genre_ids.forEach(item => {
          if(parseInt(item) === parseInt(id)){
            ret = true;
          }
        });
        return ret;
      });
      self.setState({
        movies: movies
      });
    }else{
      self.setState({
        movies: self.movies
      });
    }
  }

  componentDidMount(){
    let self = this;
    let f = new Fetch();
    //LATEST
    self.getById();
    f.get("discover/movie").then((response) => {
      let discover;
      if(!self.props.match.params.id){
        discover = {
          poster: response.results[0],
          movies: response.results
        };
        self.setState(discover);
      }else{
        discover = {
          movies: response.results
        };
        self.setState(discover);
      }
      self.setState(discover);
      self.movies = response.results;
    });
    
  }
  render (){
    let self = this;
    return (
      <div>
        <Menu parent={self}></Menu>
        <Poster item={self.state.poster}></Poster>
        <br/>
        <Movies list={self.state.movies} parent={self}></Movies>
      </div>
    );
  }
    
}

export default Discover;
